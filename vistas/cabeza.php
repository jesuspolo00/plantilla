<!DOCTYPE html>
<html lang="en">

<head>
    <title>Plantilla</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=PUBLIC_PATH?>img/icono.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Custom fonts for this template-->
    <link href="<?=PUBLIC_PATH?>vendor/fontawesome-free/css/all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?=PUBLIC_PATH?>css/bootstrapClockPicker.css">
    <!-- Custom styles for this template-->
    <link href="<?=PUBLIC_PATH?>css/sb-admin-2.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/main.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/select.css" rel="stylesheet">
    <link href="<?=PUBLIC_PATH?>css/alert.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
</head>

<body style="background-color: #ebedef;">
    <div class="loader">
    </div>
    <div id="ohsnap"></div>
    <div id="wrapper">