<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
  $er    = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia_perfil  = ControlPerfil::singleton_perfil();
$instancia_permiso = ControlPermisos::singleton_permisos();

$id_log     = $_SESSION['id'];
$perfil_log = $_SESSION['rol'];

$datos_log   = $instancia_perfil->mostrarDatosPerfilControl($id_log);
$foto_perfil = (empty($datos_log['foto_perfil'])) ? 'img/user.svg' : 'upload/' . $datos_log['foto_perfil'];
?>
<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-dark accordion bg-primary" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=BASE_URL?>inicio">
    <div class="sidebar-brand-icon">
      <i class="fas fa-leaf"></i>
    </div>
    <div class="sidebar-brand-text mx-3 text-white mt-3">
    </div>
  </a>
  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item <?=$active_inicio?> ft-texto">
    <a class="nav-link" href="<?=BASE_URL?>inicio">
      <i class="fas fa-home "></i>
      <span class="">Inicio</span>
    </a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider bg-gray my-0">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item <?=$active_usuario?> ft-texto">
    <a class="nav-link" href="<?=BASE_URL?>usuarios/index">
      <i class="fas fa-user"></i>
      <span>Usuarios</span>
    </a>
  </li>
  <!-- Divider -->
  <hr class="sidebar-divider bg-gray my-0">
  <!-- Nav Item - Dashboard -->
  <li class="nav-item <?=$active_permiso?> ft-texto">
    <a class="nav-link" href="<?=BASE_URL?>permisos/index">
      <i class="fas fa-user-lock"></i>
      <span>Permisos</span>
    </a>
  </li>
  <hr class="sidebar-divider bg-gray my-0">
  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline mt-2">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <!-- Topbar -->
    <nav class="navbar navbar-expand navbar-light topbar mb-4 static-top shadow bg-white">


      <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars text-primary"></i>
      </button>

      <!-- Topbar Navbar -->
      <ul class="navbar-nav ml-auto">

        <div class="topbar-divider d-none d-sm-block"></div>

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="mr-2 text-dark ft-texto"><?=$_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido']?></span>
            <img class="img-profile rounded-circle" src="<?=PUBLIC_PATH?><?=$foto_perfil?>">
          </a>
          <!-- Dropdown - User Information -->
          <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item ft-texto" href="<?=BASE_URL?>perfil/index">
              <i class="fas fa-user fa-sm fa-fw mr-2 text-gray"></i>
              Perfil
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item ft-texto" href="<?=BASE_URL?>salir">
              <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray"></i>
              Cerrar sesion
            </a>
          </div>
        </li>

      </ul>

    </nav>
    <!-- End of Topbar -->

    <!-- Begin Page Content -->
    <div class="container-fluid">

      <?php
      include_once VISTA_PATH . 'script_and_final.php';
    ?>