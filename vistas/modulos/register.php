<?php
require_once CONTROL_PATH . 'ControlSession.php';
include_once VISTA_PATH . 'cabeza.php';

$ingreso = ingresoClass::singleton_ingreso();
?>
<div class="container">
    <div class="card border-0 bg-transparent mt-10">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card-body border-0 bg-white rounded-left shadow mb-2">
                    <form method="POST">
                        <div class="row p-4">
                            <div class="col-lg-12 form-group text-center">
                                <h1 class="h3 text-gray-900 ft-title font-weight-bold">Reg&iacute;strate</h1>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label  class="font-weight-bold ft-title">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control ft-texto" maxlength="30" name="nombre" required placeholder="Juan" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label  class="font-weight-bold ft-title">Apellido <span class="text-danger">*</span></label>
                                <input type="text" class="form-control ft-texto" maxlength="30" name="apellido" required placeholder="Perez" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label  class="font-weight-bold ft-title">Correo electronico <span class="text-danger">*</span></label>
                                <input type="email" class="form-control ft-texto" maxlength="30" name="correo" required placeholder="ejemplo@gmail.com" required>
                            </div>
                            <div class="col-lg-12 form-group">
                                <label  class="font-weight-bold ft-title">Contrase&ntilde;a <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="password" class="form-control ft-texto password_pass_new" name="pass" minlength="8" required placeholder="************" id="password" required>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary rounded-right btn-sm ver_pass" id="pass_new" type="button">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                    </div>
                                </div>
                                <label class="text-gray-500 mt-1 ft-texto">Debe contener m&iacute;nimo 8 caracteres</label>
                            </div>
                            <div class="col-lg-12 form-group mt-d3">
                                <label  class="font-weight-bold ft-title">Confirmar contrase&ntilde;a <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="password" class="form-control ft-texto password_pass_conf" name="pass_conf" minlength="8" required placeholder="************" id="password" required>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary rounded-right btn-sm ver_pass" id="pass_conf" type="button">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                    </div>
                                </div>
                                <label class="text-gray-500 mt-1 ft-texto" id="conf_pass"></label>
                            </div>
                            <div class="col-lg-12 form-group">
                                <button class="btn btn-primary btn-block ft-title p-3" type="submit">Crear cuenta</button>
                            </div>
                            <div class="col-lg-12 form-group text-center mt-3">
                                <h6 class="ft-title">¿Ya dispones de una cuenta? &nbsp;<a href="<?=BASE_URL?>login" class="text-primary">Inicia Sesi&oacute;n</a></h6>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['correo'])) {
    $ingreso->registerUser();
}
?>
<script src="<?=PUBLIC_PATH?>js/validaciones.js"></script>