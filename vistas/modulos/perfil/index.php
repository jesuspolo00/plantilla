<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();

if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm rounded mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-white">
					<h4 class="m-0 font-weight-bold text-primary ft-title">
						<a href="<?=BASE_URL?>inicio" class="text-primary text-decoration-none">
							<i class="fas fa-arrow-left"></i>
						</a>
						&nbsp;
						Perfil
					</h4>
				</div>
				<div class="card-body">
					<form method="POST" enctype="multipart/form-data">
						<input type="hidden" name="foto_perfil" value="<?=$datos_log['foto_perfil']?>">
						<input type="hidden" name="id_user" value="<?=$id_log?>">
						<input type="hidden" name="id_log" value="<?=$id_log?>">
						<div class="row p-2">
							<div class="col-lg-12 form-group">
								<div class="row">
									<div class="col-lg-6 form-group mt-2">
										<div class="circular--portrait">
											<img src="<?=PUBLIC_PATH . $foto_perfil?>" class="img-fluid">
										</div>
									</div>
									<div class="col-lg-6 form-group mt-2">
										<div class="row">
											<div class="col-lg-12 form-group">
												<label class="font-weight-bold ft-title">Documento <span class="text-danger">*</span></label>
												<input type="number" class="form-control ft-texto numeros" name="documento" required placeholder="Numero de documento" value="<?=$datos_log['documento']?>">
											</div>
											<div class="col-lg-6 form-group">
												<label class="font-weight-bold ft-title">Nombres <span class="text-danger">*</span></label>
												<input type="text" class="form-control ft-texto" name="nombre" required placeholder="Nombres" value="<?=$datos_log['nombre']?>">
											</div>
											<div class="col-lg-6 form-group">
												<label class="font-weight-bold ft-title">Apellidos <span class="text-danger">*</span></label>
												<input type="text" class="form-control ft-texto" name="apellido" required placeholder="Apellidos" value="<?=$datos_log['apellido']?>">
											</div>
											<div class="col-lg-6 form-group">
												<label class="font-weight-bold ft-title">Telefono</label>
												<input type="number" class="form-control ft-texto numeros" name="telefono" placeholder="Telefono" value="<?=$datos_log['telefono']?>">
											</div>
											<div class="col-lg-6 form-group">
												<label class="font-weight-bold ft-title">Correo <span class="text-danger">*</span></label>
												<input type="email" class="form-control ft-texto" name="correo" required placeholder="Correo electronico" value="<?=$datos_log['correo']?>">
											</div>
											<div class="col-lg-12 form-group">
												<label class="font-weight-bold ft-title">Foto de perfil</label>
												<div class="custom-file pmd-custom-file-filled">
													<input type="file" class="custom-file-input file_input ft-texto" name="foto" id="<?=$datos_log['id_user']?>" accept=".png, .jpg, .jpeg">
													<label class="custom-file-label ft-texto file_label_<?=$datos_log['id_user']?>" for="customfilledFile"></label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-2">
								<h5 class="font-weight-bold ft-title text-primary">Cambio de contraseña</h5>
								<hr>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold ft-title">Nueva contrase&ntilde;a</label>
								<div class="input-group">
									<input type="password" class="form-control ft-texto password_pass_new" name="pass_new" placeholder="***************">
									<div class="input-group-append">
										<button class="btn btn-primary btn-sm ver_pass" id="pass_new" type="button">
											<i class="fas fa-eye"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-lg-6 form-group">
								<label class="font-weight-bold ft-title">Confirmar contrase&ntilde;a</label>
								<div class="input-group">
									<input type="password" class="form-control ft-texto password_pass_conf" name="pass_conf" placeholder="***************">
									<div class="input-group-append">
										<button class="btn btn-primary btn-sm ver_pass" id="pass_conf" type="button">
											<i class="fas fa-eye"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-lg-12 form-group text-right mt-3">
								<button class="btn btn-primary shadow-sm ft-texto" type="submit">
									<i class="fas fa-sync"></i>
									Actualizar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['id_user'])) {
	$instancia_perfil->editarPerfilControl();
}