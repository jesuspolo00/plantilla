<?php
require_once CONTROL_PATH . 'ControlSession.php';
include_once VISTA_PATH . 'cabeza.php';

$ingreso = ingresoClass::singleton_ingreso();
?>
<div class="container">
    <div class="card border-0 bg-transparent center-element">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card-body border-0 bg-white rounded-left shadow">
                    <div class="row p-4">
                        <div class="col-lg-12 form-group text-center">
                            <h1 class="h4 text-gray-900 ft-title font-weight-bold">¿Has olvidado tu contrase&ntilde;a?</h1>
                            <p class="mt-4 ft-texto">Lo entendemos, estas cosas suceden.
                            ¡Simplemente ingrese su dirección de correo electrónico a continuación y le enviaremos un enlace para restablecer su contraseña!</p>
                        </div>
                        <div class="col-lg-12 form-group">
                            <form method="POST">
                                <div class="form-group">
                                    <label class="font-weight-bold ft-title">Correo electronico</label>
                                    <input type="email" class="form-control user ft-texto" name="user" minlength="8" required placeholder="ejemplo@gmail.com" autocomplete="on">
                                </div>
                                <div class="form-group mt-4">
                                    <button class="btn btn-primary btn-block p-3 btn-sm shadow-sm ft-title">
                                        Recuperar Contrase&ntilde;a
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-12 form-group text-center mt-2">
                            <h6 class="ft-title">¿No tienes cuenta? &nbsp;<a href="<?=BASE_URL?>register" class="text-primary">Reg&iacute;strate</a></h6>
                        </div>
                        <div class="col-lg-12 form-group text-center mt-2">
                            <h6 class="ft-title">¿Ya dispones de una cuenta? &nbsp;<a href="<?=BASE_URL?>login" class="text-primary">Inicia Sesi&oacute;n</a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';

if (isset($_POST['user'])) {
    $ingreso->forgotPass();
}