<?php
require_once CONTROL_PATH . 'ControlSession.php';

$ingreso = ingresoClass::singleton_ingreso();

if (isset($_COOKIE['remember_token'])) {
	$token  = $_COOKIE['remember_token'];
	$cookie = $ingreso->cookieValidation($token);
} else if (isset($_POST['user'])) {
	$cookie = $ingreso->ingresaruser();
}

$desenc = base64_decode(@$_GET['er']);
if ($err = isset($desenc) ? $desenc : null);
include_once VISTA_PATH . 'cabeza.php';
?>
<div class="container">
	<div class="card border-0 bg-transparent login_center">
		<div class="row">
			<div class="col-lg-8 p-0 login_col">
				<div class="card-body border-0 bg-white rounded-left shadow">
					<form method="POST">
						<div class="row p-4">
							<div class="col-lg-12 form-group text-center">
								<h2 class="font-weight-bold text-primary ft-title">Welcome Back!</h2>
							</div>
							<div class="col-lg-12 form-group mt-3 ft-title">
								<label class="font-weight-bold">Correo electronico</label>
								<input type="email" class="form-control user ft-texto" name="user" required autocomplete="off" placeholder="ejemplo@gmail.com">
							</div>
							<div class="col-lg-12 form-group">
								<label class="font-weight-bold ft-title">Contrase&ntilde;a</label>
								<div class="input-group">
									<input type="password" class="form-control ft-texto password_pass" name="pass" required autocomplete="off" placeholder="***********">
									<div class="input-group-append">
										<button class="btn btn-primary rounded-right btn-sm ver_pass" id="pass" type="button">
											<i class="fas fa-eye"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="col-lg-12 form-group">
								<div class="row">
									<div class="col-lg-6 form-group">
										<div class="form-check">
											<input class="form-check-input" type="checkbox" value="1" name="remember" id="flexCheckDefault">
											<label class="form-check-label ft-title fs-09em" for="flexCheckDefault">
												Recuerdame
											</label>
										</div>
									</div>
									<div class="col-lg-6 form-group">
										<a href="<?=BASE_URL?>password" class="text-right ft-title fs-09em">¿Olvidaste tu contrase&ntilde;a?</a>
									</div>
								</div>
							</div>
							<div class="col-lg-12 form-group mt-1">
								<button class="btn btn-primary btn-block p-3 btn-sm shadow-sm ft-title">
									Iniciar Sesi&oacute;n
								</button>
							</div>
						</div>
					</form>
					<div class="col-lg-12 text-center mt-d3 mb-10">
						<h6 class="ft-title">¿No tienes cuenta? &nbsp;<a href="<?=BASE_URL?>register" class="text-primary">Reg&iacute;strate</a></h6>
						<?php
						switch ($err) {
							case 1:
							echo '<p class="text-danger mt-4 text-center ft-texto">Usuario o Contrase&ntilde;a Incorrecta</p>';
							break;
							case 2:
							echo '<p class="text-danger mt-4 text-center ft-texto">Usuario o Contrase&ntilde;a Incorrecta</p>';
							break;
							case 3:
							echo '<p class="text-danger mt-4 text-center ft-texto">Usuario o Contrase&ntilde;a Incorrecta</p>';
							break;
							case 4:
							echo '<p class="text-danger mt-4 text-center ft-texto">No esta permitido iniciar sesion</p>';
							break;
							case 5:
							echo '<p class="text-danger mt-4 text-center ft-texto">Debes iniciar sesion para acceder</p>';
							break;
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-lg-6 p-0 logo_col">
				<div class="card-body border-0 bg-login-netzen rounded-right shadow">
					<div class="row">
						<div class="col-lg-12 form-group text-center mt-30">
							<img src="<?=PUBLIC_PATH?>img/logo.png" class="img-fluid" width="300" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';