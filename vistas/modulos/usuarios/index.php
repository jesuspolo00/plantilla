<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}

$active_usuario = 'active';

include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia = ControlUsuarios::singleton_usuario();

$datos_perfil = $instancia_perfil->mostrarPerfilesControl();

if (isset($_POST['buscar'])) {

} else {
	$datos_usuarios = $instancia->mostrarUsuariosControl();
}

$permiso_acceso   = $instancia_permiso->consultarPermisoControl($perfil_log, 2);
$permiso_editar   = $instancia_permiso->consultarPermisoControl($perfil_log, 3);
$permiso_eliminar = $instancia_permiso->consultarPermisoControl($perfil_log, 4);

if (!$permiso_acceso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm rounded mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-white">
					<h4 class="m-0 font-weight-bold text-primary ft-title">
						<a href="<?=BASE_URL?>inicio" class="text-primary text-decoration-none">
							<i class="fas fa-arrow-left"></i>
						</a>
						&nbsp;
						Usuarios
					</h4>
					<div class="btn-group">
						<button class="btn btn-primary btn-sm ft-texto" type="button" data-toggle="modal" data-target="#nuevo_usuario">
							<i class="fas fa-plus"></i>
							Nuevo
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row p-2">
							<div class="col-lg-8 form-group"></div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control ft-texto filtro" placeholder="Buscar..." name="buscar">
									<div class="input-group-append">
										<button class="btn btn-primary ft-texto" type="submit">
											<i class="fas fa-search"></i>
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm">
							<thead class="bg-light">
								<tr class="text-center ft-texto">
									<th>Documento</th>
									<th>Nombre Completo</th>
									<th>Telefono</th>
									<th>Correo</th>
									<th>Perfil</th>
									<th>Estado</th>
									<th></th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $usuarios) {
									$id_usuario   = $usuarios['id_user'];
									$documento    = $usuarios['documento'];
									$nom_completo = $usuarios['nombre'] . ' ' . $usuarios['apellido'];
									$telefono     = $usuarios['telefono'];
									$correo       = $usuarios['correo'];
									$nom_perfil   = $usuarios['nom_perfil'];

									$foto_usuario = (empty($usuarios['foto_perfil'])) ? 'img/user.svg' : 'upload/' . $usuarios['foto_perfil'];

									$span_estado = ($usuarios['estado'] == 0) ? '<span class="badge badge-pill badge-danger ft-texto">Inactivo</span>' : '<span class="badge badge-pill badge-success ft-texto">Activo</span>';

									?>
									<tr class="text-center ft-texto">
										<td><?=$documento?></td>
										<td><?=$nom_completo?></td>
										<td><?=$telefono?></td>
										<td><?=$correo?></td>
										<td><?=$nom_perfil?></td>
										<td><?=$span_estado?></td>
										<td>
											<div class="btn-group">
												<?php if ($permiso_editar) {?>
													<button class="btn btn-primary btn-sm" type="button" data-tooltip="tooltip" data-trigger="hover" title="Editar" data-placement="bottom" data-toggle="modal" data-target="#editar_usuario_<?=$id_usuario?>">
														<i class="fas fa-user-edit"></i>
													</button>
												<?php }if ($permiso_eliminar) {?>
													<button class="btn btn-danger btn-sm" type="button" data-tooltip="tooltip" title="Eliminar" data-placement="bottom">
														<i class="fas fa-trash"></i>
													</button>
												<?php }?>
											</div>
										</td>
									</tr>

									<div class="modal fade" id="editar_usuario_<?=$id_usuario?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title font-weight-bold text-primary ft-title" id="exampleModalLabel">Editar usuario</h5>
												</div>
												<div class="modal-body">
													<form method="POST" enctype="multipart/form-data">
														<input type="hidden" name="foto_perfil" value="<?=$usuarios['foto_perfil']?>">
														<input type="hidden" name="id_user" value="<?=$id_usuario?>">
														<input type="hidden" name="id_log" value="<?=$id_log?>">
														<div class="row p-3">
															<div class="col-lg-12 form-group">
																<div class="row mb-d4">
																	<div class="col-lg-6 form-group mt-4">
																		<div class="circular--modal">
																			<img src="<?=PUBLIC_PATH . $foto_usuario?>" class="img-fluid">
																		</div>
																	</div>
																	<div class="col-lg-6 form-group mt-3">
																		<div class="row">
																			<div class="col-lg-12 form-group">
																				<label class="font-weight-bold ft-title">Documento <span class="text-danger">*</span></label>
																				<input type="number" class="form-control ft-texto numeros" name="documento" required placeholder="Numero de documento" value="<?=$usuarios['documento']?>">
																			</div>
																			<div class="col-lg-6 form-group">
																				<label class="font-weight-bold ft-title">Nombres <span class="text-danger">*</span></label>
																				<input type="text" class="form-control ft-texto" name="nombre" required placeholder="Nombres" value="<?=$usuarios['nombre']?>">
																			</div>
																			<div class="col-lg-6 form-group">
																				<label class="font-weight-bold ft-title">Apellidos <span class="text-danger">*</span></label>
																				<input type="text" class="form-control ft-texto" name="apellido" required placeholder="Apellidos" value="<?=$usuarios['apellido']?>">
																			</div>
																			<div class="col-lg-12 form-group">
																				<label class="font-weight-bold ft-title">Telefono</label>
																				<input type="number" class="form-control ft-texto numeros" name="telefono" placeholder="Telefono" value="<?=$usuarios['telefono']?>">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold ft-title">Correo <span class="text-danger">*</span></label>
																<input type="email" class="form-control ft-texto" name="correo" required placeholder="Correo electronico" value="<?=$usuarios['correo']?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold ft-title">Perfil <span class="text-danger">*</span></label>
																<select name="perfil" class="form-control ft-texto" required>
																	<option value="" selected>Seleccione una opci&oacute;n...</option>
																	<?php
																	foreach ($datos_perfil as $perfil) {
																		$id_perfil  = $perfil['id_perfil'];
																		$nom_perfil = $perfil['nombre'];

																		$selected_perfil = ($id_perfil == $usuarios['perfil']) ? 'selected' : '';

																		if ($id_perfil != 1) {
																			?>
																			<option value="<?=$id_perfil?>" <?=$selected_perfil?>><?=$nom_perfil?></option>
																			<?php
																		}
																	}
																	?>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold ft-title">Estado <span class="text-danger">*</span></label>
																<select name="estado" class="form-control" required>
																	<?php
																	$activo_select   = ($usuarios['estado'] == 1) ? 'selected' : '';
																	$inactivo_select = ($usuarios['estado'] == 0) ? 'selected' : '';
																	?>
																	<option value="1" <?=$activo_select?>>Activo</option>
																	<option value="0" <?=$inactivo_select?>>Inactivo</option>
																</select>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold ft-title">Foto de perfil</label>
																<div class="custom-file pmd-custom-file-filled">
																	<input type="file" class="custom-file-input file_input ft-texto" name="foto" id="<?=$usuarios['id_user']?>" accept=".png, .jpg, .jpeg">
																	<label class="custom-file-label ft-texto file_label_<?=$usuarios['id_user']?>" for="customfilledFile"></label>
																</div>
															</div>
															<div class="col-lg-12 form-group mt-3">
																<h5 class="font-weight-bold ft-title text-primary">Cambio de contraseña</h5>
																<hr>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold ft-title">Nueva contrase&ntilde;a</label>
																<div class="input-group">
																	<input type="password" class="form-control ft-texto password_pass_new_<?=$id_usuario?>" name="pass_new" placeholder="***************">
																	<div class="input-group-append">
																		<button class="btn btn-primary btn-sm ver_pass" id="pass_new_<?=$id_usuario?>" type="button">
																			<i class="fas fa-eye"></i>
																		</button>
																	</div>
																</div>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold ft-title">Confirmar contrase&ntilde;a</label>
																<div class="input-group">
																	<input type="password" class="form-control ft-texto password_pass_conf_<?=$id_usuario?>" name="pass_conf" placeholder="***************">
																	<div class="input-group-append">
																		<button class="btn btn-primary btn-sm ver_pass" id="pass_conf_<?=$id_usuario?>" type="button">
																			<i class="fas fa-eye"></i>
																		</button>
																	</div>
																</div>
															</div>
															<div class="col-lg-12 form-group text-right mt-3">
																<button class="btn btn-secondary shadow-sm ft-texto" type="button" data-dismiss="modal">
																	<i class="fas fa-times"></i>
																	Cancelar
																</button>
																<button class="btn btn-primary shadow-sm ft-texto" type="submit">
																	<i class="fas fa-sync"></i>
																	Actualizar
																</button>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>

								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';

if (isset($_POST['id_log_reg'])) {
	$instancia->agregarUsuarioControl();
}

if (isset($_POST['id_user'])) {
	$instancia_perfil->editarPerfilControl();
}