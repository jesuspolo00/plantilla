<div class="modal fade" id="nuevo_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold ft-title text-primary" id="exampleModalLabel">Nuevo usuario</h5>
      </div>
      <div class="modal-body">
        <form method="POST" enctype="multipart/form-data">
          <input type="hidden" name="id_log_reg" value="<?=$id_log?>">
          <div class="row p-3">
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Documento <span class="text-danger">*</span></label>
              <input type="number" class="form-control ft-texto numeros" name="documento" required placeholder="Numero de documento">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Nombres <span class="text-danger">*</span></label>
              <input type="text" class="form-control ft-texto" name="nombre" required placeholder="Nombres">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Apellidos <span class="text-danger">*</span></label>
              <input type="text" class="form-control ft-texto" name="apellido" required placeholder="Apellidos">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Telefono</label>
              <input type="number" class="form-control ft-texto numeros" name="telefono" placeholder="Numero de telefono">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Correo <span class="text-danger">*</span></label>
              <input type="email" class="form-control ft-texto" name="correo" required placeholder="Correo electronico">
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Perfil <span class="text-danger">*</span></label>
              <select name="perfil" class="form-control ft-texto" required>
                <option value="" selected>Seleccione una opci&oacute;n...</option>
                <?php
                foreach ($datos_perfil as $perfil) {
                  $id_perfil  = $perfil['id_perfil'];
                  $nom_perfil = $perfil['nombre'];

                  if ($id_perfil != 1) {
                    ?>
                    <option value="<?=$id_perfil?>"><?=$nom_perfil?></option>
                    <?php
                  }
                }
                ?>
              </select>
            </div>
            <div class="col-lg-12 form-group">
              <label class="font-weight-bold ft-title">Foto de perfil</label>
              <div class="custom-file pmd-custom-file-filled">
                <input type="file" class="custom-file-input file_input ft-texto" name="foto" id="" accept=".png, .jpg, .jpeg">
                <label class="custom-file-label ft-texto file_label_" for="customfilledFile"></label>
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Nueva contrase&ntilde;a <spa class="text-danger">*</spa></label>
              <div class="input-group">
                <input type="password" class="form-control ft-texto password_pass_new" name="pass_new" placeholder="***************" required>
                <div class="input-group-append">
                  <button class="btn btn-primary btn-sm ver_pass" id="pass_new" type="button">
                    <i class="fas fa-eye"></i>
                  </button>
                </div>
              </div>
            </div>
            <div class="col-lg-6 form-group">
              <label class="font-weight-bold ft-title">Confirmar contrase&ntilde;a <spa class="text-danger">*</spa></label>
              <div class="input-group">
                <input type="password" class="form-control ft-texto password_pass_conf" name="pass_conf" placeholder="***************" required>
                <div class="input-group-append">
                  <button class="btn btn-primary btn-sm ver_pass" id="pass_conf" type="button">
                    <i class="fas fa-eye"></i>
                  </button>
                </div>
              </div>
            </div>
            <div class="col-lg-12 form-group mt-2 text-right">
              <button class="btn btn-secondary shadow-sm ft-texto" type="button" data-dismiss="modal">
                <i class="fas fa-times"></i>
                Cancelar
              </button>
              <button class="btn btn-primary shadow-sm ft-texto" type="submit">
                <i class="fas fa-save"></i>
                Guardar
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
