<?php
require_once CONTROL_PATH . 'ControlSession.php';
include_once VISTA_PATH . 'cabeza.php';

$ingreso = ingresoClass::singleton_ingreso();

if (isset($_GET['token'])) {

	$variable = $_GET['token'];
	$explode  = explode("__", $variable);

	$token     = $explode[1];
	$id_user   = base64_decode($explode[0]);
	$fecha_hoy = date('Y-m-d H:i:s');

	$verificar_token = $ingreso->tokenVerifyControl($id_user, $token);

	if (!empty($verificar_token['id'])) {
		if ($fecha_hoy <= $verificar_token['fecha_fin'] && $verificar_token['activo'] == 1) {
			?>
			<div class="container">
				<div class="card border-0 bg-transparent center-element">
					<div class="row">
						<div class="col-lg-3"></div>
						<div class="col-lg-6">
							<div class="card-body border-0 bg-white rounded-left shadow">
								<div class="row p-4">
									<div class="col-lg-12 form-group text-center">
										<h1 class="h4 text-gray-900 ft-title font-weight-bold">Crea una nueva contrase&ntilde;a</h1>
										<p class="mt-4 ft-texto">Tu nueva contrase&ntilde;a debe ser distinta a la contraseña utilizada anteriormente.</p>
									</div>
									<div class="col-lg-12 form-group">
										<form method="POST">
											<input type="hidden" name="id_user" value="<?=$id_user?>">
											<input type="hidden" name="token" value="<?=$token?>">
											<div class="row">
												<div class="col-lg-12 form-group">
													<label class="font-weight-bold ft-title">Nueva Contrase&ntilde;a</label>
													<input type="password" class="form-control ft-texto" name="pass" id="password" required placeholder="**********" autocomplete="on">
													<label class="text-gray-500 mt-1 ft-texto">Debe contener m&iacute;nimo 8 caracteres</label>
												</div>
												<div class="col-lg-12 form-group mt-d2">
													<label  class="font-weight-bold ft-title">Confirmar contrase&ntilde;a <span class="text-danger">*</span></label>
													<input type="password" class="form-control ft-texto" name="pass_conf" minlength="8" required placeholder="************" id="conf_password" required>
													<label class="text-gray-500 mt-1 ft-texto" id="conf_pass"></label>
												</div>
												<div class="col-lg-12 form-group mt-1">
													<button class="btn btn-primary btn-block p-3 btn-sm shadow-sm ft-title">
														Restablecer la contrase&ntilde;a
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
			include_once VISTA_PATH . 'script_and_final.php';

			if (isset($_POST['pass'])) {
				$ingreso->newPassUserControl();
			}
		} else {
			?>
			<div class="container-fluid">
				<div class="text-center">
					<div class="error m-auto" data-text="404">404</div>
					<p class="lead text-gray-800 mb-5">Pagina no encontrada!</p>
					<p class="text-gray-500 mb-0">Pagina no encontrada</p>
					<a href="<?=BASE_URL?>inicio">&larr; Volver al inicio</a>
				</div>
			</div>
			<?php
		}
	}
}
?>
<script src="<?=PUBLIC_PATH?>js/validaciones.js"></script>