<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}

$active_permiso = 'active';

include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil = $instancia_perfil->mostrarPerfilesControl();

$permiso_acceso = $instancia_permiso->consultarPermisoControl($perfil_log, 1);
if (!$permiso_acceso) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-white">
					<h4 class="m-0 font-weight-bold text-primary ft-title">
						<a href="<?=BASE_URL?>inicio" class="text-primary text-decoration-none">
							<i class="fas fa-arrow-left"></i>
						</a>
						&nbsp;
						Permisos
					</h4>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row p-2">
							<div class="col-lg-8 form-group"></div>
							<div class="col-lg-4 form-group">
								<div class="input-group mb-3">
									<input type="text" class="form-control ft-texto filtro" placeholder="Buscar..." name="buscar">
									<div class="input-group-append">
										<button class="btn btn-primary ft-texto" type="submit">
											<i class="fas fa-search"></i>
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-hover border table-sm">
							<thead class="bg-light">
								<tr class="text-center ft-texto">
									<th>Perfil</th>
									<th>Cantidad de accesos</th>
									<th>Estado</th>
									<th></th>
								</tr>
							</thead>
							<tbody class="buscar">
								<?php
								foreach ($datos_perfil as $perfil) {
									$id_perfil         = $perfil['id_perfil'];
									$nom_perfil        = $perfil['nombre'];
									$estado            = $perfil['estado'];
									$cantidad_accesos  = $perfil['cantidad_accesos'];
									$cantidad_opciones = $perfil['cantidad_opciones'];

									$estado_span     = ($estado == 0) ? '<span class="badge badge-pill badge-danger ft-texto">Inactivo</span>' : '<span class="badge badge-pill badge-success ft-texto">Activo</span>';
									$estado_cantidad = '<span class="badge badge-pill badge-primary ft-texto">' . $cantidad_accesos . '/' . $cantidad_opciones . '</span>';

									?>
									<tr class="text-center ft-texto">
										<td><?=$nom_perfil?></td>
										<td><?=$estado_cantidad?></td>
										<td><?=$estado_span?></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-primary btn-sm" type="button" data-tooltip="tooltip" title="Detalles" data-placement="bottom" data-trigger="hover">
													<i class="fas fa-info-circle"></i>
												</button>
											</div>
										</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/permisos/funcionesPermisos.js"></script>