$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*-------------------------*/
    $(".inactivar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        inactivarUsuario(id);
    });
    $(".buscar").on("click", ".inactivar", function() {
        var id = $(this).attr('id');
        inactivarUsuario(id);
    });
    /*---------------------------*/
    $(".activar").on(tipoEvento, function() {
        var id = $(this).attr('id');
        activarUsuario(id)
    });
    $(".buscar").on("click", ".activar", function() {
        var id = $(this).attr('id');
        activarUsuario(id)
    });
    /*--------------------------------------------*/
    $("#doc_user").keypress(function(e) {
        if (event.which == 13) {
            var documento = $(this).val();
            if (documento == '') {
                ohSnap("Documento esta vacio!", {
                    color: "red",
                    'duration': '1000'
                });
                $("#doc_user").focus();
            } else {
                consultarDocumento(documento);
            }
        }
    });
    /* $("#doc_user").focusout(function(e) {
         var documento = $(this).val();
         if (documento == '') {
             ohSnap("Documento esta vacio!", {
                 color: "red",
                 'duration': '1000'
             });
             $("#doc_user").focus();
         } else {
             consultarDocumento(documento);
         }
     });*/
    /*-------------------------------------------------*/
    function inactivarUsuario(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/inactivar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('#' + id).removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-danger btn-sm inactivar').addClass('btn btn-success btn-sm activar');
                        $('#' + id + ' i').removeClass('fa-times').addClass('fa-check');
                        ohSnap("Inactivado correctamente!", {
                            color: "yellow",
                            'duration': '1000'
                        });
                    } else {
                        ohSnap("Error al inactivar!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------------------------------*/
    function activarUsuario(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/activar.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $('#' + id).removeAttr('title');
                        $('#' + id + '').removeClass('btn btn-success btn-sm activar').addClass('btn btn-danger btn-sm inactivar');
                        $('#' + id + ' i').removeClass('fa-check').addClass('fa-times');
                        ohSnap("Activado correctamente!", {
                            color: "green",
                            'duration': '1000'
                        });
                    } else {
                        ohSnap("Error al activar!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-------------------------------------------------*/
    function consultarDocumento(id) {
        try {
            $.ajax({
                url: '../vistas/ajax/usuarios/documento.php',
                method: 'POST',
                data: {
                    'id': id
                },
                cache: false,
                success: function(resultado) {
                    if (resultado == 'ok') {
                        $("input").attr('disabled', false);
                        $("select").attr('disabled', false);
                        ohSnap("Documento no resgitrado!", {
                            color: "green",
                            'duration': '1000'
                        });
                    } else {
                        ohSnap("Documento ya registrado!", {
                            color: "red",
                            'duration': '1000'
                        });
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
});