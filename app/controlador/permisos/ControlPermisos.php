<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'permisos' . DS . 'ModeloPermisos.php';

class ControlPermisos
{

    private static $instancia;

    public static function singleton_permisos()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function consultarPermisoControl($perfil, $opcion)
    {
        $mostrar = ModeloPermisos::consultarPermisoModel($perfil, $opcion);
        return $mostrar;
    }
}
