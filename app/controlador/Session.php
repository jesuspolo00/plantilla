<?php
header('Content-Type: text/html; charset=utf-8');
require_once MODELO_PATH . 'IngresoModel.php';

class Session
{

    public function iniciar()
    {
        @session_start();
    }

    public function setsession($nombre, $valor)
    {

        $_SESSION[$nombre] = $valor;
    }

    public function outsession()
    {
        IngresoModel::tokenSesionCookie($_SESSION['id'], '');
        session_unset();
        session_destroy();
        setcookie("remember_token", "", time() - 604800, "/");
    }

}
