<?php

function guardarVariosArchivos($file, $i)
{

    if (isset($file['name'][$i]) && !empty($file['name'][$i])) {

        $archivo      = $file['name'][$i];
        $ext_original = pathinfo($archivo, PATHINFO_EXTENSION);
        $ext_arch     = pathinfo($archivo, PATHINFO_EXTENSION);
        $ext_arch     = ($ext_arch == 'JPG') ? 'jpg' : $ext_arch;
        $ext_arch     = ($ext_arch == 'jpg') ? 'jpeg' : $ext_arch;
        $fecha_arch   = date('YmdHis');

        $nombre_archivo = strtolower(md5(rand(5, 9999) . '_' . $fecha_arch)) . '.' . $ext_arch;

        $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
        $ruta_img     = $carp_destino . $nombre_archivo;

        if ($ext_arch == 'png' || $ext_arch == 'jpeg') {
            $compressed = compressImage($file['tmp_name'][$i], $ruta_img, 50, $ext_original);
        } else {

            if (is_uploaded_file($file['tmp_name'][$i])) {
                move_uploaded_file($file['tmp_name'][$i], $ruta_img);
            }
        }
    }

    return (!empty($nombre_archivo)) ? $nombre_archivo : '';
}

function guardarArchivo($archivo)
{
    $nom_arch     = $archivo['name'];
    $ext_original = pathinfo($nom_arch, PATHINFO_EXTENSION);
    $ext_arch     = pathinfo($nom_arch, PATHINFO_EXTENSION);
    $ext_arch     = ($ext_arch == 'JPG') ? 'jpg' : $ext_arch;
    $ext_arch     = ($ext_arch == 'jpg') ? 'jpeg' : $ext_arch;
    $fecha_arch   = date('YmdHis');

    $nombre_archivo = strtolower(md5(rand(5, 9999) . '_' . $fecha_arch)) . '.' . $ext_arch;

    $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
    $ruta_img     = $carp_destino . $nombre_archivo;

    if ($ext_arch == 'png' || $ext_arch == 'jpeg') {
        $compressed = compressImage($archivo['tmp_name'], $ruta_img, 50, $ext_original);
    } else {
        if (is_uploaded_file($archivo['tmp_name'])) {
            move_uploaded_file($archivo['tmp_name'], $ruta_img);
        }
    }

    return $nombre_archivo;
}

function eliminarArchivo($archivo)
{
    $ruta = PUBLIC_PATH_ARCH . 'upload' . DS . $archivo;

    if (!empty($archivo)) {
        if (unlink($ruta)) {
            $rs = true;
        } else {
            $rs = false;
        }
    } else {
        $rs = true;
    }

    return $rs;
}

function compressImage($source, $destination, $quality, $validate)
{
    // Obtenemos la información de la imagen
    $imgInfo = getimagesize($source);
    $mime    = $imgInfo['mime'];

    // Creamos una imagen
    switch ($mime) {
        case 'image/jpeg':
        $image = imagecreatefromjpeg($source);
        if ($validate == 'JPG') {
            $image = imagerotate($image, 90, 0);
        }
        break;
        case 'image/png':
        $image = imagecreatefrompng($source);
        break;
        case 'image/gif':
        $image = imagecreatefromgif($source);
        break;
        default:
        $image = imagecreatefromjpeg($source);
    }

    // Guardamos la imagen
    imagejpeg($image, $destination, $quality);

    // Devolvemos la imagen comprimida
    return $destination;
}

function mesesEspanol($mes)
{
    $meses = array("Todos", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    return $meses[$mes];
}

function diasEspanol($dia)
{
    $diassemana = array("Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sábado", "Domingo");
    return $diassemana[$dia];
}

function fechaCastellano($fecha)
{
    $fecha     = substr($fecha, 0, 10);
    $numeroDia = date('d', strtotime($fecha));
    $dia       = date('l', strtotime($fecha));
    $mes       = date('F', strtotime($fecha));
    $anio      = date('Y', strtotime($fecha));
    $dias_ES   = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
    $dias_EN   = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
    $nombredia = str_replace($dias_EN, $dias_ES, $dia);
    $meses_ES  = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses_EN  = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
    return $nombredia . " " . $numeroDia . " de " . $nombreMes . " de " . $anio;
}

function convertirnumeroletra($numero)
{
    $numf = milmillon($numero);
    return strtoupper($numf);
}

function milmillon($nummierod)
{
    if ($nummierod >= 1000000000 && $nummierod < 2000000000) {
        $num_letrammd = "mil " . (cienmillon($nummierod % 1000000000));
    }
    if ($nummierod >= 2000000000 && $nummierod < 10000000000) {
        $num_letrammd = unidad(floor($nummierod / 1000000000)) . " mil " . (cienmillon($nummierod % 1000000000));
    }
    if ($nummierod < 1000000000) {
        $num_letrammd = cienmillon($nummierod);
    }

    return $num_letrammd;
}

function cienmillon($numcmeros)
{
    if ($numcmeros == 100000000) {
        $num_letracms = "cien millones";
    }

    if ($numcmeros >= 100000000 && $numcmeros < 1000000000) {
        $num_letracms = centena(floor($numcmeros / 1000000)) . " millones " . (millon($numcmeros % 1000000));
    }
    if ($numcmeros < 100000000) {
        $num_letracms = decmillon($numcmeros);
    }

    return $num_letracms;
}

function decmillon($numerodm)
{
    if ($numerodm == 10000000) {
        $num_letradmm = "diez millones";
    }

    if ($numerodm > 10000000 && $numerodm < 20000000) {
        $num_letradmm = decena(floor($numerodm / 1000000)) . "millones " . (cienmiles($numerodm % 1000000));
    }
    if ($numerodm >= 20000000 && $numerodm < 100000000) {
        $num_letradmm = decena(floor($numerodm / 1000000)) . " millones " . (millon($numerodm % 1000000));
    }
    if ($numerodm < 10000000) {
        $num_letradmm = millon($numerodm);
    }

    return $num_letradmm;
}

function millon($nummiero)
{
    if ($nummiero >= 1000000 && $nummiero < 2000000) {
        $num_letramm = "un millon " . (cienmiles($nummiero % 1000000));
    }
    if ($nummiero >= 2000000 && $nummiero < 10000000) {
        $num_letramm = unidad(floor($nummiero / 1000000)) . " millones " . (cienmiles($nummiero % 1000000));
    }
    if ($nummiero < 1000000) {
        $num_letramm = cienmiles($nummiero);
    }

    return $num_letramm;
}

function cienmiles($numcmero)
{
    if ($numcmero == 100000) {
        $num_letracm = "cien mil";
    }

    if ($numcmero >= 100000 && $numcmero < 1000000) {
        $num_letracm = centena(floor($numcmero / 1000)) . " mil " . (centena($numcmero % 1000));
    }
    if ($numcmero < 100000) {
        $num_letracm = decmiles($numcmero);
    }

    return $num_letracm;
}

function decmiles($numdmero)
{
    if ($numdmero == 10000) {
        $numde = "diez mil";
    }

    if ($numdmero > 10000 && $numdmero < 20000) {
        $numde = decena(floor($numdmero / 1000)) . "mil " . (centena($numdmero % 1000));
    }
    if ($numdmero >= 20000 && $numdmero < 100000) {
        $numde = decena(floor($numdmero / 1000)) . " mil " . (miles($numdmero % 1000));
    }
    if ($numdmero < 10000) {
        $numde = miles($numdmero);
    }

    return $numde;
}

function miles($nummero)
{
    if ($nummero >= 1000 && $nummero < 2000) {
        $numm = "mil " . (centena($nummero % 1000));
    }
    if ($nummero >= 2000 && $nummero < 10000) {
        $numm = unidad(floor($nummero / 1000)) . " mil " . (centena($nummero % 1000));
    }
    if ($nummero < 1000) {
        $numm = centena($nummero);
    }

    return $numm;
}

function centena($numc)
{
    if ($numc >= 100) {
        if ($numc >= 900 && $numc <= 999) {
            $numce = "novecientos ";
            if ($numc > 900) {
                $numce = $numce . (decena($numc - 900));
            }

        } else if ($numc >= 800 && $numc <= 899) {
            $numce = "ochocientos ";
            if ($numc > 800) {
                $numce = $numce . (decena($numc - 800));
            }

        } else if ($numc >= 700 && $numc <= 799) {
            $numce = "setecientos ";
            if ($numc > 700) {
                $numce = $numce . (decena($numc - 700));
            }

        } else if ($numc >= 600 && $numc <= 699) {
            $numce = "seiscientos ";
            if ($numc > 600) {
                $numce = $numce . (decena($numc - 600));
            }

        } else if ($numc >= 500 && $numc <= 599) {
            $numce = "quinientos ";
            if ($numc > 500) {
                $numce = $numce . (decena($numc - 500));
            }

        } else if ($numc >= 400 && $numc <= 499) {
            $numce = "cuatrocientos ";
            if ($numc > 400) {
                $numce = $numce . (decena($numc - 400));
            }

        } else if ($numc >= 300 && $numc <= 399) {
            $numce = "trescientos ";
            if ($numc > 300) {
                $numce = $numce . (decena($numc - 300));
            }

        } else if ($numc >= 200 && $numc <= 299) {
            $numce = "doscientos ";
            if ($numc > 200) {
                $numce = $numce . (decena($numc - 200));
            }

        } else if ($numc >= 100 && $numc <= 199) {
            if ($numc == 100) {
                $numce = "cien ";
            } else {
                $numce = "ciento " . (decena($numc - 100));
            }

        }
    } else {
        $numce = decena($numc);
    }

    return $numce;
}

function decena($numdero)
{

    if ($numdero >= 90 && $numdero <= 99) {
        $numd = "noventa ";
        if ($numdero > 90) {
            $numd = $numd . "y " . (unidad($numdero - 90));
        }

    } else if ($numdero >= 80 && $numdero <= 89) {
        $numd = "ochenta ";
        if ($numdero > 80) {
            $numd = $numd . "y " . (unidad($numdero - 80));
        }

    } else if ($numdero >= 70 && $numdero <= 79) {
        $numd = "setenta ";
        if ($numdero > 70) {
            $numd = $numd . "y " . (unidad($numdero - 70));
        }

    } else if ($numdero >= 60 && $numdero <= 69) {
        $numd = "sesenta ";
        if ($numdero > 60) {
            $numd = $numd . "y " . (unidad($numdero - 60));
        }

    } else if ($numdero >= 50 && $numdero <= 59) {
        $numd = "cincuenta ";
        if ($numdero > 50) {
            $numd = $numd . "y " . (unidad($numdero - 50));
        }

    } else if ($numdero >= 40 && $numdero <= 49) {
        $numd = "cuarenta ";
        if ($numdero > 40) {
            $numd = $numd . "y " . (unidad($numdero - 40));
        }

    } else if ($numdero >= 30 && $numdero <= 39) {
        $numd = "treinta ";
        if ($numdero > 30) {
            $numd = $numd . "y " . (unidad($numdero - 30));
        }

    } else if ($numdero >= 20 && $numdero <= 29) {
        if ($numdero == 20) {
            $numd = "veinte ";
        } else {
            $numd = "veinti" . (unidad($numdero - 20));
        }

    } else if ($numdero >= 10 && $numdero <= 19) {
        switch ($numdero) {
            case 10:{
                $numd = "diez ";
                break;
            }
            case 11:{
                $numd = "once ";
                break;
            }
            case 12:{
                $numd = "doce ";
                break;
            }
            case 13:{
                $numd = "trece ";
                break;
            }
            case 14:{
                $numd = "catorce ";
                break;
            }
            case 15:{
                $numd = "quince ";
                break;
            }
            case 16:{
                $numd = "dieciseis ";
                break;
            }
            case 17:{
                $numd = "diecisiete ";
                break;
            }
            case 18:{
                $numd = "dieciocho ";
                break;
            }
            case 19:{
                $numd = "diecinueve ";
                break;
            }
        }
    } else {
        $numd = unidad($numdero);
    }

    return $numd;
}

function unidad($numuero)
{
    switch ($numuero) {
        case 9:{
            $numu = "nueve";
            break;
        }
        case 8:{
            $numu = "ocho";
            break;
        }
        case 7:{
            $numu = "siete";
            break;
        }
        case 6:{
            $numu = "seis";
            break;
        }
        case 5:{
            $numu = "cinco";
            break;
        }
        case 4:{
            $numu = "cuatro";
            break;
        }
        case 3:{
            $numu = "tres";
            break;
        }
        case 2:{
            $numu = "dos";
            break;
        }
        case 1:{
            $numu = "un";
            break;
        }
        case 0:{
            $numu = "";
            break;
        }
    }
    return $numu;
}
