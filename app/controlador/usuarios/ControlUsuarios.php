<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'hash.php';

class ControlUsuarios
{

    private static $instancia;

    public static function singleton_usuario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarUsuariosControl()
    {
        $mostrar = ModeloUsuarios::mostrarUsuariosModel();
        return $mostrar;
    }

    public function mostrarUsuariosBuscarControl($buscar)
    {
        $mostrar = ModeloUsuarios::mostrarUsuariosBuscarControl($buscar);
        return $mostrar;
    }

    public function agregarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log_reg']) &&
            !empty($_POST['id_log_reg'])
        ) {

            $pass = Hash::hashpass($_POST['documento']);

            $datos = array(
                'id_log'    => $_POST['id_log_reg'],
                'documento' => $_POST['documento'],
                'nombre'    => $_POST['nombre'],
                'apellido'  => $_POST['apellido'],
                'correo'    => $_POST['correo'],
                'telefono'  => $_POST['telefono'],
                'perfil'    => $_POST['perfil'],
                'pass'      => $pass,
            );

            $guardar = ModeloUsuarios::agregarUsuarioModel($datos);

            if ($guardar['guardar'] == true) {
                $title   = 'Usuario registrado';
                $message = 'Se ha registrado con exito.';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }
}
