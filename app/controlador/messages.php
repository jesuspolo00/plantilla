<?php

function alertSuccess($title, $message, $enlace)
{
    $alerta = '
	<script>
	swal({
		title: "' . $title . '",
		text: " ' . $message . '",
		icon: "success",
		type: "success",
		button: false,
		closeOnClickOutside: false,
		timer: 2300,
		}).then((result) => {
			if (!result) {
				window.location.replace("' . $enlace . '");
			}
			});
			</script>
			';

    echo $alerta;
}

function alertWarning($title, $message, $enlace)
{
    $alerta = '
	<script>
	swal({
		title: "' . $title . '",
		text: " ' . $message . '",
		icon: "warning",
		type: "warning",
		button: false,
		closeOnClickOutside: false,
		timer: 2300,
		}).then((result) => {
			if (!result) {
				window.location.replace("' . $enlace . '");
			}
			});
			</script>
			';

    echo $alerta;
}

function alertDanger($title, $message, $enlace)
{
    $alerta = '
	<script>
	swal({
		title: "' . $title . '",
		text: " ' . $message . '",
		icon: "error",
		type: "danger",
		button: false,
		closeOnClickOutside: false,
		timer: 2300,
		}).then((result) => {
			if (!result) {
				window.location.replace("' . $enlace . '");
			}
			});
			</script>
			';

    echo $alerta;
}

function forgotPassMessage($enlace)
{

    $font_title = "font-family: 'Helvetica';";
    $font_text  = "font-family: 'Trebuchet MS';";

    $message = '
	<body style="padding: 3px; background-color: #D5D8DC; width: 100%; overflow: hidden; ' . $font_title . '">
	<center>
	<div style="background-color: transparent; height: auto; width: 530px;">
	<table border="0" style="width: 100%; border-collapse:collapse; border: none;" cellpadding="0" cellspacing="0">
	<tr>
	<td style="width: 530px; height: 75px; flex-shrink: 0; fill: #1268D5; background-color: #1268D5;border-top-right-radius: 1.2em; border-top-left-radius: 1.2em; margin-top: 15%; text-align: center; padding: 35px;">
	<img src="https://hebreo.hajsoft.co/public/img/prueba/logo.png" style="width: 120px;" alt="">
	</td>
	</tr>
	<tr>
	<td style="width: 530px; height: 666px; flex-shrink: 0; background-color: white;">
	<center>
	<p style="width: 530px; height: 36px; flex-shrink: 0; text-align:center;  margin-top: -15px; margin-bottom: 115px;">
	<span style="color: #1268D5;text-align: center;text-shadow: 5px 5px 8px rgba(0, 0, 0, 0.10);' . $font_text . 'font-size: 32px;font-style: normal;font-weight: 700;line-height: normal;">¿Olvidaste tu contrase&ntilde;a?</span>
	<br>
	<span style="color: #1268D5;text-align: center;text-shadow: 5px 5px 8px rgba(0, 0, 0, 0.10);' . $font_text . 'font-size: 22px;font-style: normal;font-weight: 400;line-height: normal;">No te preocupes, estas cosas pasan.</span>
	</p>
	<p style="width: 378px;height: 69px;flex-shrink: 0; text-align:center;">
	<span style="color: #707070;text-align: center;' . $font_text . 'font-size: 16px;font-style: normal;font-weight: 400;line-height: normal;">Est&aacute; recibiendo este correo electr&oacute;nico porque hemos recibido una solicitud de restablecimiento de contrase&ntilde;a para su cuenta</span>
	</p>
	<a href="' . $enlace . '" style="display: inline-flex;padding: 17px 20px;justify-content: center;align-items: center;gap: 10px; margin-top: 37px; border-radius: 0.5em;background: #1268D5;box-shadow: 3px 3px 7px 0px rgba(0, 0, 0, 0.18); text-decoration: none; color: white; ' . $font_text . '">Restablecer contrase&ntilde;a</a>
	<p style="width: 370px; margin-top: 66px;">
	<img src="https://hebreo.hajsoft.co/public/img/history.png" alt="" style="width: 40px;height: 40px;flex-shrink: 0; margin-bottom: 17px;">
	<br>
	<span style="color: #F35858;text-align: center;' . $font_text . 'font-size: 18px;font-style: normal;font-weight: 700;line-height: normal;">Este enlace vence en 30 minutos</span>
	</p>
	<p style="width: 368px;height: 34px;flex-shrink: 0; margin-top: 83px;">
	<span style="color: #707070;text-align: center;' . $font_text . 'font-size: 16px;font-style: normal;font-weight: 400;line-height: normal;">Si no ha solicitado un restablecimiento de contrase&ntilde;a, no es necesario realizar otra acci&oacute;n</span>
	</p>
	</center>
	</td>
	</tr>
	<tr>
	<td style="width: 530px; height: 75px; flex-shrink: 0; fill: #1268D5; background-color: #1268D5;border-bottom-right-radius: 1.2em; border-bottom-left-radius: 1.2em; margin-top: 15%; text-align: center;">
	<h4 style="color: #FFF;' . $font_text . 'font-size: 20px;font-style: normal;font-weight: 700;line-height: normal; padding-top: 1%;">&copy; Copyright by Netzen Studio 2023</h4>
	</td>
	</tr>
	</table>
	</div>
	</center>
	</body>
	';

    return $message;
}
