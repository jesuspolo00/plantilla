<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'hash.php';
require_once CONTROL_PATH . 'numeros.php';
require_once CONTROL_PATH . 'messages.php';

class ControlPerfil
{

    private static $instancia;

    public static function singleton_perfil()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarDatosPerfilControl($id)
    {
        $datos = ModeloPerfil::mostrarDatosPerfilModel($id);
        return $datos;
    }

    public function mostrarPerfilesControl()
    {
        $datos = ModeloPerfil::mostrarPerfilesModel();
        return $datos;
    }

    public function editarPerfilControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {

            $nom_foto = $_POST['foto_perfil'];

            if (isset($_FILES['foto']['name']) && !empty($_FILES['foto']['name'])) {
                eliminarArchivo($nom_foto);
                $nom_foto = guardarArchivo($_FILES['foto']);
            }

            $datos = array(
                'id_user'     => $_POST['id_user'],
                'documento'   => $_POST['documento'],
                'nombre'      => $_POST['nombre'],
                'apellido'    => $_POST['apellido'],
                'correo'      => $_POST['correo'],
                'telefono'    => $_POST['telefono'],
                'foto_perfil' => $nom_foto,
                'id_log'      => $_POST['id_log'],
            );

            $guardar = ModeloPerfil::editarPerfilModel($datos);

            if ($guardar == true) {

                if (isset($_POST['estado'])) {

                    $fecha = ($_POST['estado'] == 1) ? ' fecha_activo = NOW() ' : ' fecha_inactivo = NOW() ';

                    $datos_estado = array(
                        'id_user' => $_POST['id_user'],
                        'estado'  => $_POST['estado'],
                        'fecha'   => $fecha,
                    );

                    $actualizar_estado = ModeloPerfil::actualizarEstadoModel($datos_estado);
                }

                if (isset($_POST['perfil']) && !empty($_POST['perfil'])) {

                    $datos_perfil = array(
                        'id_user' => $_POST['id_user'],
                        'perfil'  => $_POST['perfil'],
                    );

                    $actualizar_perfil = ModeloPerfil::actualizarPerfilUsuarioModel($datos_perfil);
                }

                if (isset($_POST['pass_new']) && isset($_POST['pass_conf'])) {

                    $pass_new  = $_POST['pass_new'];
                    $pass_conf = $_POST['pass_conf'];

                    if (!empty($pass_new) && !empty($pass_conf)) {

                        if ($pass_new === $pass_conf) {

                            $pass_codificada = Hash::hashpass($pass_conf);

                            $datos_pass = array(
                                'id_user' => $_POST['id_user'],
                                'pass'    => $pass_codificada,
                            );

                            $actualizar_pass = ModeloPerfil::actualizarPasswordModel($datos_pass);

                        } else {
                            $title   = 'Contraseñas no coinciden';
                            $message = 'Las contraseñas deben coincidir, intente nuevamente.';
                            $enlace  = 'index';
                            alertDanger($title, $message, $enlace);
                            die();
                        }
                    }
                }

                $title   = 'Datos actualizados';
                $message = 'Se ha actualizado la informacion';
                $enlace  = 'index';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualizacion';
                $message = 'Lo sentimos ha ocurrido un error, se solucionara lo antes posible.';
                $enlace  = '';
                alertDanger($title, $message, $enlace);
            }
        }
    }

}
