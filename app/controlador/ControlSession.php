<?php
require_once MODELO_PATH . 'IngresoModel.php';
require_once CONTROL_PATH . 'Session.php';
require_once CONTROL_PATH . 'hash.php';
require_once MODELO_PATH . 'configMail.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreo.php';
include_once CONTROL_PATH . 'messages.php';

class ingresoClass
{

    private static $instancia;
    private $objsession;

    public static function singleton_ingreso()
    {

        if (!isset(self::$instancia)) {

            $miclase = __CLASS__;

            self::$instancia = new $miclase;
        }

        return self::$instancia;
    }

    public function tokenVerifyControl($id, $token)
    {
        $mostrar = IngresoModel::tokenVerifyModel($id, $token);
        return $mostrar;
    }

    public function ingresaruser()
    {
        if (
            isset($_POST['user']) &&
            !empty($_POST['user']) &&
            isset($_POST['pass']) &&
            !empty($_POST['pass'])
        ) {

            $user = filter_var($_POST['user']);
            $pass = filter_var($_POST['pass']);
            $rslt = IngresoModel::verificarUser($user);

            if ($rslt) {
                if ($rslt['estado'] == 1) {
                    if ($rslt['correo'] === $user) {
                        $hash = $rslt['pass'];
                        if (Hash::verificar($hash, $pass)) {

                            if (!empty($_POST['remember'])) {
                                $token = bin2hex(random_bytes(32)); // Genera un token aleatorio
                                // Guarda el token en la base de datos asociado al usuario
                                $token_cookie = IngresoModel::tokenSesionCookie($rslt['id_user'], $token);
                                // Cookie válida por una semana
                                setcookie("remember_token", $token, [
                                    'expires'  => time() + 604800,
                                    'path'     => '/',
                                    'domain'   => null,
                                    'secure'   => false, //Cambiar cuando sea HTTPS
                                    'httponly' => true,
                                    'samesite' => 'Lax',
                                ]);
                            }

                            $this->objsession = new Session;
                            $this->objsession->iniciar();
                            $this->objsession->SetSession('id', $rslt['id_user']);
                            $this->objsession->SetSession('nombre_admin', $rslt['nombre']);
                            $this->objsession->SetSession('apellido', $rslt['apellido']);
                            $this->objsession->SetSession('rol', $rslt['perfil']);
                            header('Location: inicio');
                        } else {
                            $er    = '1';
                            $error = base64_encode($er);
                            header('Location:login?er=' . $error);
                        }
                    } else {
                        $er    = '2';
                        $error = base64_encode($er);
                        header('Location:login?er=' . $error);
                    }
                } else {
                    $er    = '4';
                    $error = base64_encode($er);
                    header('Location:login?er=' . $error);
                }
            } else {
                $er    = '3';
                $error = base64_encode($er);
                header('Location:login?er=' . $error);
            }
        } else {
        }
    }

    public function cookieValidation($token)
    {
        if (!empty($token)) {

            $usuario = IngresoModel::verificarToken($token);

            if ($usuario) {
                // Autentica al usuario y establece la sesión
                $this->objsession = new Session;
                $this->objsession->iniciar();
                $this->objsession->SetSession('id', $usuario['id_user']);
                $this->objsession->SetSession('nombre_admin', $usuario['nombre']);
                $this->objsession->SetSession('apellido', $usuario['apellido']);
                $this->objsession->SetSession('rol', $usuario['perfil']);

                // Redirige al usuario a la página de inicio
                header('Location: inicio');
            } else {
                $er    = '5';
                $error = base64_encode($er);
                header('Location:login?er=' . $error);
            }

        }
    }

    public function forgotPass()
    {
        if (isset($_POST['user']) &&
            !empty($_POST['user'])
        ) {

            $user = filter_var($_POST['user']);

            $rslt = IngresoModel::verificarUser($user);

            if (!empty($rslt)) {

                if ($rslt['correo'] == $user) {

                    $token   = bin2hex(random_bytes(32)); // Genera un token aleatorio
                    $id_user = base64_encode($rslt['id_user']);

                    $fecha_inicio = date('Y-m-d H:i:s');
                    $fecha_vence  = strtotime('+30 minute', strtotime($fecha_inicio));
                    $fecha_vence  = date('Y-m-d H:i:s', $fecha_vence);

                    $datos_pass = array(
                        'id_user'      => $rslt['id_user'],
                        'token'        => $token,
                        'fecha_inicio' => $fecha_inicio,
                        'fecha_vence'  => $fecha_vence,
                    );

                    $pass_recover = IngresoModel::tokenRecoverUserPassModel($datos_pass);

                    $token_cryp = $id_user . '__' . $token;
                    $enlace     = BASE_URL . 'recover_pass?token=' . $token_cryp;

                    $datos = array(
                        'asunto'  => 'Recuperar contraseña',
                        'mensaje' => forgotPassMessage($enlace),
                        'correo'  => array($rslt['correo']),
                        'archivo' => array(''),
                    );

                    $envio = Correo::enviarCorreoModel($datos);

                    if ($envio == true) {
                        $title   = 'Correo Enviado';
                        $message = 'Te hemos enviado un enlace a tu correo!';
                        $enlace  = BASE_URL . 'login';
                        alertSuccess($title, $message, $enlace);
                    } else {
                        $title   = 'Error de envio';
                        $message = 'Lo sentimos ha ocurrido un error, lo solucionaremos lo antes posible.';
                        $enlace  = BASE_URL . 'password';
                        alertDanger($title, $message, $enlace);
                    }

                }

            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos este correo no esta en el sistema :(';
                $enlace  = BASE_URL . 'password';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function registerUser()
    {
        if (isset($_POST['correo']) &&
            !empty($_POST['correo'])
        ) {

            $pass_hash = ($_POST['pass'] == $_POST['pass_conf']) ? Hash::hashpass($_POST['pass_conf']) : '';

            if (empty($pass_hash)) {
                $title   = 'Problema al registrar';
                $message = 'Las contraseñas no coinciden, por favor vuelve a diligenciar el formulario.';
                $enlace  = BASE_URL . 'register';
                alertWarning($title, $message, $enlace);
                die();
            }

            $datos = array(
                'nombre'   => $_POST['nombre'],
                'apellido' => $_POST['apellido'],
                'correo'   => $_POST['correo'],
                'pass'     => $pass_hash,
                'perfil'   => 2,
                'user_log' => 0,
            );

            $registro = IngresoModel::registerUserModel($datos);

            if ($registro['guardar'] == true) {

                /*$token   = bin2hex(random_bytes(32)); // Genera un token aleatorio
                $id_user = base64_encode($registro['id']);

                $token_cryp = $id_user . '__' . $token;
                $enlace     = BASE_URL . 'verification?token=' . $token_cryp;*/

                $title   = 'Registro Exitoso';
                $message = 'Se ha enviado un enlace de verificacion a tu correo!';
                $enlace  = BASE_URL . 'login';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de registro';
                $message = 'Lo sentimos ha ocurrido un error, lo solucionaremos lo antes posible.';
                $enlace  = BASE_URL . 'register';
                alertDanger($title, $message, $enlace);
            }

        }
    }

    public function newPassUserControl()
    {
        if (isset($_POST['pass']) &&
            !empty($_POST['pass'])
        ) {

            $token   = $_POST['token'];
            $id_user = base64_encode($_POST['id_user']);

            $token_cryp = $id_user . '__' . $token;

            $pass_hash = ($_POST['pass'] == $_POST['pass_conf']) ? Hash::hashpass($_POST['pass_conf']) : '';

            if (empty($pass_hash)) {
                $title   = 'Problema al restablecer';
                $message = 'Las contraseñas no coinciden, por favor vuelve a diligenciar el formulario.';
                $enlace  = BASE_URL . 'recover_pass?token=' . $token_cryp;
                alertWarning($title, $message, $enlace);
                die();
            }

            $datos = array(
                'id_user' => $_POST['id_user'],
                'pass'    => $pass_hash,
            );

            $reset = IngresoModel::newPassUserModel($datos);

            if ($reset == true) {
                $title   = 'Cambio Exitoso';
                $message = 'Se ha realizado el cambio de contraseña!';
                $enlace  = BASE_URL . 'login';
                alertSuccess($title, $message, $enlace);
            } else {
                $title   = 'Error de actualización';
                $message = 'Lo sentimos ha ocurrido un error, lo solucionaremos lo antes posible.';
                $enlace  = BASE_URL . 'login';
                alertDanger($title, $message, $enlace);
            }
        }
    }
}
