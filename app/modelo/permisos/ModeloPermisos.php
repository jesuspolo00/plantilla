<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPermisos extends conexion
{

    public static function consultarPermisoModel($perfil, $opcion)
    {
        $tabla  = 'permisos_perfil';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM permisos_perfil WHERE id_perfil = :p AND id_opcion = :op AND estado = 1 ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':p', $perfil);
            $preparado->bindParam(':op', $opcion);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
