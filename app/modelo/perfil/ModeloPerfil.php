<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloPerfil extends conexion
{

    public static function mostrarDatosPerfilModel($id)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarPerfilesModel()
    {
        $tabla  = 'perfiles';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        p.id_perfil,
        p.nombre,
        p.estado,
        (SELECT COUNT(pp.id) FROM permisos_perfil pp WHERE pp.id_perfil = p.id_perfil AND pp.estado = 1) AS cantidad_accesos,
        (SELECT COUNT(po.id) FROM permisos_opcion po WHERE po.estado = 1) AS cantidad_opciones
        FROM perfiles p;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarPerfilModel($datos)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET documento = :d, nombre = :n, apellido = :ap, telefono = :t, correo = :cr, foto_perfil = :ft, user_log = :idl WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ap', $datos['apellido']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':cr', $datos['correo']);
            $preparado->bindParam(':ft', $datos['foto_perfil']);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarPerfilUsuarioModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET perfil = :p WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':p', $datos['perfil']);
            $preparado->bindParam(':id', $datos['id_user']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarEstadoModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET estado = '" . $datos['estado'] . "', " . $datos['fecha'] . " WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $datos['id_user']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function actualizarPasswordModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET pass = :p WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':p', $datos['pass']);
            $preparado->bindParam(':id', $datos['id_user']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

}
