<?php

require_once 'conexion.php';

class IngresoModel extends conexion
{

    public static function verificarUser($nick)
    {
        $cnx   = conexion::singleton_conexion();
        $tabla = 'usuarios';
        $cmd   = "SELECT * FROM " . $tabla . " WHERE correo = :us;";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindParam(':us', $nick);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function tokenSesionCookie($id, $token)
    {
        $cnx   = conexion::singleton_conexion();
        $tabla = 'usuarios';
        $cmd   = "UPDATE " . $tabla . " SET token = '" . $token . "' WHERE id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function verificarToken($token)
    {
        $cnx   = conexion::singleton_conexion();
        $tabla = 'usuarios';
        $cmd   = "SELECT * FROM " . $tabla . " WHERE token = :t;";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindParam(':t', $token);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function registerUserModel($datos)
    {
        $cnx   = conexion::singleton_conexion();
        $tabla = 'usuarios';
        $cmd   = "INSERT INTO " . $tabla . " (nombre, apellido, correo, pass, perfil, user_log) VALUES (:n, :ap, :c, :pass, :p, :idl);";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ap', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':pass', $datos['pass']);
            $preparado->bindParam(':p', $datos['perfil']);
            $preparado->bindParam(':idl', $datos['user_log']);
            if ($preparado->execute()) {
                $id  = $cnx->ultimoIngreso($tabla);
                $rsl = array('guardar' => true, 'id' => $id);
                return $rsl;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function tokenRecoverUserPassModel($datos)
    {
        $cnx   = conexion::singleton_conexion();
        $tabla = 'recover_pass';
        $cmd   = "UPDATE " . $tabla . " SET activo = 0 WHERE id_user = :id;
        INSERT INTO " . $tabla . " (id_user, token, fecha_inicio, fecha_fin) VALUES (:id, :tk, :fi, :fv);";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':tk', $datos['token']);
            $preparado->bindParam(':fi', $datos['fecha_inicio']);
            $preparado->bindParam(':fv', $datos['fecha_vence']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function tokenVerifyModel($id, $token)
    {
        $cnx   = conexion::singleton_conexion();
        $tabla = 'recover_pass';
        $cmd   = "SELECT * FROM " . $tabla . " WHERE id_user = :id and token = :tk;";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindParam(':id', $id);
            $preparado->bindParam(':tk', $token);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function newPassUserModel($datos)
    {
        $cnx   = conexion::singleton_conexion();
        $tabla = 'usuarios';
        $cmd   = "UPDATE " . $tabla . " SET pass = :p WHERE id_user = :id;
        UPDATE recover_pass SET activo = 0 WHERE id_user: id;";
        try {
            $preparado = $cnx->preparar($cmd);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':p', $datos['pass']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
