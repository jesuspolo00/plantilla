<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloUsuarios extends conexion
{

    public static function mostrarUsuariosModel()
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM " . $tabla . " u
        LEFT JOIN perfiles p ON p.id_perfil = u.perfil
        WHERE p.id_perfil NOT IN(1)
        ORDER BY u.id_user DESC LIMIT 20;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarUsuariosBuscarControl($buscar)
    {
        $tabla  = 'usuarios';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        u.*,
        p.nombre AS nom_perfil
        FROM " . $tabla . " u
        LEFT JOIN perfiles p ON p.id = u.perfil
        WHERE CONCAT(u.nombre, ' ' , u.apellido, ' ', u.correo,  ' ', u.telefono, ' ', u.user) LIKE '%" . $buscar . "%';";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarUsuarioModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "INSERT INTO " . $tabla . " (documento, nombre, apellido, correo, telefono, perfil, pass, user_log) VALUES (:d, :n, :a, :c, :t, :p, :pass, :idl);";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':p', $datos['perfil']);
            $preparado->bindParam(':pass', $datos['pass']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                $id  = $cnx->ultimoIngreso($tabla);
                $rsl = array('id' => $id, 'guardar' => true);
                return $rsl;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarUsuarioModel($datos)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET documento = :d, nombre = :n, apellido = :a, correo = :c, telefono = :t, perfil = :r, user_log = :idl WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindValue(':id', $datos['id_user']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':d', $datos['documento']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':a', $datos['apellido']);
            $preparado->bindParam(':c', $datos['correo']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':r', $datos['perfil']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarUsuarioModel($id)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET activo = 0, fecha_inactivo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarUsuarioModel($id)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET activo = 1, fecha_activo = NOW() WHERE id_user = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function consultarDocumentoModel($id)
    {
        $tabla = 'usuarios';
        $cnx   = conexion::singleton_conexion();
        $sql   = "SELECT * FROM " . $tabla . " WHERE documento = '" . $id . "' ORDER BY id_user DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($sql);
            //$preparado->bindParam(':d', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
